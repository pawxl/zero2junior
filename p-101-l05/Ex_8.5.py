#Exercise_1
#"q" or "Q" breaks a loop
while True:
    user_input = input("Type 'q' or 'Q' to exit: ")
    if user_input.lower() == "q":
        break

#Exercise_2
#Write numbers not multiplies of 3 from 1 through 50.

for i in range(1, 51):
    if i % 3 == 0:
        continue
    print(i)
