#Exercise_1
#create tuple
cardinal_numbers = "first", "second", "third"

#Exercise_2
#print second tuple
print(cardinal_numbers[1])

#Exercise_3
#unpack and print
position1, position2, position3 = cardinal_numbers
print(position1)
print(position2)
print(position3)

#Exercise_4
#create tuple contains your name
my_name = tuple("Pawel")

#Exercise_5
#search for "x" in name
print("x" in my_name)

#Exercise_6
#print name without first letter
changed_name = my_name[1:]
print(changed_name)
