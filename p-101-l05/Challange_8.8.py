#Simulate a coin toss experiment
import random

#simulates coin flip
def coin_flip():
    if random.randint(0, 1) == 0:
        return "heads"
    else:
        return "tails"

    
num_flips = 10000
flip = 0


for trail in range(num_flips):
    #assign first value and iterates
    first_flip = coin_flip() 
    flip = flip + 1

    #checks if coin lands in the same position
    #iterates flips  
    while coin_flip() == first_flip:
        flip = flip + 1
    #coin landed in different position
    flip = flip + 1

avr_flips_number = flip / num_flips
print(f"Average number of flips per trail is: {avr_flips_number}")
