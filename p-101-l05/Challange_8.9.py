import random

num_A_wins = 0
num_B_wins = 0

num_trials = 10_000
for trail in range(num_trials):
    
    candidate_A = 0
    candidate_B = 0

    #Region_1
    if random.random() < 0.87:
        candidate_A = candidate_A + 1
    else:
        candidate_B = candidate_B + 1
        
    #Region_2
    if random.random() < 0.65:
        candidate_A = candidate_A + 1
    else:
        candidate_B = candidate_B + 1
        
    #Region_3
    if random.random() < 0.17:
        candidate_A = candidate_A + 1
    else:
        candidate_B = candidate_B + 1

    #who wins
    if candidate_A > candidate_B:
            num_A_wins = num_A_wins + 1
    else:
            num_B_wins = num_B_wins + 1
print(f"Probability Candidate A wins: {(num_A_wins / num_trials)*100:,.2f}")
print(f"Probability Candidate B wins: {(num_B_wins / num_trials)*100:,.2f}")
