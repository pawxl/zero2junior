import random
#Exercise_1
def roll():
    return random.randint(1, 6)
    
print(roll())

#Exercise_2
num_rolls = 10_000
dice_value = 0
for trial in range(num_rolls):
    dice_value = dice_value + roll()
average_value = dice_value / num_rolls
print(f"average result of {num_rolls} rolls is {average_value}")
