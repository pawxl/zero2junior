#Program receives two numbers from the user and displays first number raise to the power of second
base = input("Enter a base: ")
exponent = input("Enter an exponent: ")
result = float(base) ** float(exponent)
print(f"{base} to the power of {exponent} = {result}")
