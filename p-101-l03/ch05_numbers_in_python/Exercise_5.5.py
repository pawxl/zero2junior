#Exercise_1
#Asks user for number and displays that number rounded to two decimal places

user_input = input("Enter a number: ")
rounded_input  = round(float(user_input), 2)
print(f"{user_input} rounded to 2 decimal places is {rounded_input}")

#Exercise_2
#Asks user to iput a number and then displays the absolute value of number.

user_input = input("Enter a number: ")
result = abs(float(user_input))
print(f"The absolute value of {user_input} is {result}")

#Exercise_3
#Asks the user to input two number, then displays whether differce between two is an intiger
#
user_input1 = input("Enter a number: ")
user_input2 = input("Enter another number: ")
num1 = float(user_input1)
num2 = float(user_input2)
result = num1 - num2
result.is_integer()
print(f"The differnce between {num1} and {num2} is an intiger? {result.is_integer()} ")
