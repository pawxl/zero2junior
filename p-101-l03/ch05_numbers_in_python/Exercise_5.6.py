#Exercise_1
#print the result of the calculation 3 ** .125
#as a fixed-point number with three decimal places

num = 3 ** 0.125
print(f"{num:.3f}")

#Exercise_2

num = 150000
print(f"${num:,.2f}")

#Exercise_3

num = 2 / 10
print(f"{num:.0%}")
