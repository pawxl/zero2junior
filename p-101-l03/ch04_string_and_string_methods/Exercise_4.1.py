#excercise_1
print('and then she said "i can not do this anymore"')

#excercise_2
print("What's on your mind?")

#excercise_3
print("""This string spans multiple lines
with whitespace
      preserved""")

#excercise_4
print("This string is coded on \
multiple lines \
but get printed on a single line")
