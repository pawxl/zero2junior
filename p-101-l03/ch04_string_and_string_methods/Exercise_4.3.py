#Exercise_1
string_1 = "Animals"
string_2 = "Badger"
string_3 = "Honey Bee"
string_4= "Honey Badger"

print(string_1.lower())
print(string_2.lower())
print(string_3.lower())
print(string_4.lower())

#Exercise_2
print(string_1.upper())
print(string_2.upper())
print(string_3.upper())
print(string_4.upper())

#Exercise_3
string1 = "    Filet Mignon"
string2 = "Brisket   "
string3 = "    Cheeseburger   "
print(string1.lstrip())
print(string2.rstrip())
print(string3.strip())

#Exercise_4
string1 = "Becomes"
string2 = "becomes"
string3 = "BEAR"
string4 = "    bEautiful"

print(string1.startswith("be"))
print(string2.startswith("be"))
print(string3.startswith("be"))
print(string4.startswith("be"))

#Exercise_5
string1 = string1.lower()
string3 = string3.lower()
string4 = string4.strip().lower()

print(string1.startswith("be"))
print(string2.startswith("be"))
print(string3.startswith("be"))
print(string4.startswith("be"))













