#Exercise_1
#loop that prints out integers 2 through 10, each on a new line
for n in range(2, 11):
    print(n)

print("\n")
#Exercise_2
#the same program but using while loop
n = 2
while (n < 11):
    print(n)
    n = n + 1

print("\n")
#Exercise_3
#function called doubles() that takes a number as its input and doubles it.
#loop doubles() to double the number three times, displaying each results on seperate line.

def doubles(x):
    doubled_x = x * 2
    return doubled_x

num = 2
for i in range(3):
    num = doubles(num)
    print(num)

