#Exercise_1
def cube(x):
    """ It returns value of number raised to the third power"""
    result = x ** 3
    return result
print(f"7 cube is {cube(7)}")
print(f"13 cube is {cube(13)}")

#Exercise_2
def greet(name):
    """Display a greeting"""
    print(f"Hello {name}!")

greet("Pawel")
