#Convert Temperatures

def convert_cel_to_far(C):
    F = C * 9/5 + 32
    return F

def convert_far_to_cel(F):
    C = (F - 32) * 5/9
    return C

user_input = input("Enter a temparute in degrees F: ")
num = float(user_input)
print(f"{num} degrees F = {convert_far_to_cel(num):.2f} degrees C")

user_input2 = input("\nEnter a temperature id degrees C: ")
num2 = float(user_input2)
print(F"{num2} degrees C = {convert_cel_to_far(num2):.2f} degrees F")
