#Program tracks the growing amount of an investment over time



def invest(amount, rate, years):
    for year in range(1, years + 1):
        amount = amount + (amount * rate)
        print(f"year {year}: ${amount:,.2f}")
        
amount = float(input("Enter principal amount: "))
rate = float(input("Enter annual rate of return: "))
years = int(input("Enter number of years to calculate: "))

invest(amount, rate, years)
